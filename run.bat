@echo off
:: Config file path : %userprofile%\AppData\Roaming\Seafile Updater
:: Log file path : %userprofile%\Logging

:: Please alpways check seafile path
:: Seafile home path = C:\src\seafile\
:: Subdirectory = scripts\msi_updater

:: Case 1
:: When prompted
:: Does C:\src\seafile\scripts\msi_updater specify a file name
:: or directory name on the target
:: (F = file, D = directory)? D

:: Case 2
:: When prompted
:: Overwrite C:\src\seafile\scripts\msi_updater\bz2.pyd (Yes/No/All)? All

:: product launcher
python pyinstaller/pyinstaller.py --icon=app.ico --version-file=asfs-open-version.txt --onefile --windowed -m=open-asfs.manifest open-asfs.py

:: Running the python code to generate execuatable
::pyinstaller.exe --onefile --windowed app.py
python pyinstaller/pyinstaller.py --icon=app.ico --version-file=version.txt --windowed -m=updater.manifest updater.py

:: Copy install.bat to msi_updater
:: xcopy updater.manifest dist\updater\

:: Copy cmd to copy from src to dest
xcopy /e dist\updater\* C:\src\seafile\scripts\msi_updater

:: Copy win-updater-op.bat to msi_updater
xcopy win-updater-op.bat C:\src\seafile\scripts\msi_updater

:: Copy win-updater-op.bat to msi_updater
xcopy dist\open-asfs.exe C:\src\seafile\scripts\msi_updater

:: Delete the intermediate generated files
rmdir build /s /q
rmdir dist /s /q
del *.spec
echo Press enter to exit
set /p input=