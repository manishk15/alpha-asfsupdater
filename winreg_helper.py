# -*- coding: utf-8 -*-

"""
    Copyright
    -----------------------------------------------------------------------------
    Copyright (C) Talentica Software , Inc - All Rights Reserved Unauthorized 
    copying of this file, via any medium is strictly prohibited
    -----------------------------------------------------------------------------

    FileName :      winreg_helper.py
    Created Date :  21-12-2016
    Created by :    MrityunjayK mkumar@alpha-sense.com
"""
'''
if sys.platform == 'win32':
    import _winreg

    _registry = _winreg.ConnectRegistry(None, _winreg.HKEY_CURRENT_USER)


    def get_runonce():
        return _winreg.OpenKey(_registry,
                               r"Software\Microsoft\Windows\CurrentVersion\Run", 0,
                               _winreg.KEY_ALL_ACCESS)


    def add(name, application):
        """add a new autostart entry"""
        key = get_runonce()
        _winreg.SetValueEx(key, name, 0, _winreg.REG_SZ, application)
        _winreg.CloseKey(key)


    def exists(name):
        """check if an autostart entry exists"""
        key = get_runonce()
        exists = True
        try:
            _winreg.QueryValueEx(key, name)
        except WindowsError:
            exists = False
        _winreg.CloseKey(key)
        return exists


    def remove(name):
        """delete an autostart entry"""
        key = get_runonce()
        _winreg.DeleteValue(key, name)
        _winreg.CloseKey(key)
else:
    _xdg_config_home = os.environ.get("XDG_CONFIG_HOME", "~/.config")
    _xdg_user_autostart = os.path.join(os.path.expanduser(_xdg_config_home),
                                       "autostart")


    def getfilename(name):
        """get the filename of an autostart (.desktop) file"""
        return os.path.join(_xdg_user_autostart, name + ".desktop")


    def add(name, application):
        """add a new autostart entry"""
        desktop_entry = "[Desktop Entry]\n" \
                        "Name=%s\n" \
                        "Exec=%s\n" \
                        "Type=Application\n" \
                        "Terminal=false\n" % (name, application)
        with open(getfilename(name), "w") as f:
            f.write(desktop_entry)


    def exists(name):
        """check if an autostart entry exists"""
        return os.path.exists(getfilename(name))


    def remove(name):
        """delete an autostart entry"""
        os.unlink(getfilename(name))
'''

import sys
import _winreg

AUTO_RUN_LOCATION = r"Software\Microsoft\Windows\CurrentVersion\Run"

"""
    Shared Variables
"""

"""
    Local Functions
"""

"""
    Global Functions
"""

"""
    Class Definitions
"""


class AutoStartRegistry:
    def __init__(self):
        self._registry = _winreg.ConnectRegistry(None, _winreg.HKEY_CURRENT_USER)

    def __get_run_once__(self):
        return _winreg.OpenKey(self._registry, AUTO_RUN_LOCATION, 0, _winreg.KEY_ALL_ACCESS)

    def add(self, name, application):
        """add a new autostart entry"""
        key = self.__get_run_once__()
        _winreg.SetValueEx(key, name, 0, _winreg.REG_SZ, application)
        _winreg.CloseKey(key)

    def exists(self, name):
        """check if an autostart entry exists"""
        key = self.__get_run_once__()
        exists = True
        try:
            _winreg.QueryValueEx(key, name)
        except WindowsError:
            exists = False
        _winreg.CloseKey(key)
        return exists

    def remove(self, name):
        """delete an autostart entry"""
        key = self.__get_run_once__()
        _winreg.DeleteValue(key, name)
        _winreg.CloseKey(key)


def test():
    if sys.platform == 'win32':
        asr = AutoStartRegistry()
        try:
            if asr.exists("test_xxx"):
                pass
            else:
                asr.add("test_xxx", "test")
        finally:
            assert asr.exists("test_xxx")


def remove_test():
    if sys.platform == 'win32':
        asr = AutoStartRegistry()
        try:
            if asr.exists("test_xxx"):
                asr.remove("test_xxx")
        finally:
            assert not asr.exists("test_xxx")