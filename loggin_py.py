# -*- coding: utf-8 -*-

"""
    Copyright
    -----------------------------------------------------------------------------
    Copyright (C) Talentica Software , Inc - All Rights Reserved Unauthorized 
    copying of this file, via any medium is strictly prohibited
    -----------------------------------------------------------------------------

    FileName :      loggin_py.py
    Created Date :  30-11-2016
    Created by :    MrityunjayK mkumar@alpha-sense.com
"""
import os
import logging
from logging.handlers import RotatingFileHandler

"""
    Shared Variables
"""

"""
    Local Functions
"""

"""
    Global Functions
"""

"""
    Class Definitions
"""
LOGGING_DIRECTORY = os.path.expandvars('%APPDATA%/ASLogging')
LOG_FILE_NAME = "updater-service.log"
if not os.path.exists(LOGGING_DIRECTORY):
    os.makedirs(LOGGING_DIRECTORY)
Logfile = LOGGING_DIRECTORY + "/" + LOG_FILE_NAME
# FORMAT = filename + "#%(levelname)s#%(asctime)s#%(funcName)s#%(lineno)d#%(message)s"
log_formatter = logging.Formatter(
    '%(process)d %(asctime)s %(levelname)s %(filename)s %(funcName)s(%(lineno)d) %(message)s')
custom_log_handler = RotatingFileHandler(Logfile, mode='a', maxBytes=5 * 1024 * 1024,
                                         backupCount=5, encoding=None, delay=0)
custom_log_handler.setFormatter(log_formatter)
custom_log_handler.setLevel(logging.INFO)

from wmi import WMI
wmi_instance = WMI()


def get_installed_programs_list():
    for each in wmi_instance.Win32_Product():
        yield each


def get_logger(file_name):
    app_log = logging.getLogger(file_name)
    app_log.setLevel(logging.DEBUG)
    app_log.addHandler(custom_log_handler)
    return app_log
