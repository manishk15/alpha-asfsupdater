@echo off
set workDir=%1
cd %workDir%
echo %workDir%
if exist "%workDir%\updater.exe" (	
	tasklist /fi "imagename eq msi_updater.exe" |find ":" > nul
	if errorlevel 1 taskkill /f /im "msi_updater.exe" & DEL /f msi_updater.exe
	copy updater.exe msi_updater.exe
	DEL /f updater.exe
	start msi_updater.exe
)
cd ..


