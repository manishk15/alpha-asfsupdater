# Run this file in elevated UAC

from ConfigHandler import loadConfig, saveConfig, timeSinceLastCheck, confExisted, INTERVAL_IN_HRS
from subprocess import Popen, PIPE, pywintypes
import win32com.shell.shell as shell
import tempfile, platform
import urllib2, re, os, sys
import time
from loggin_py import get_logger, get_installed_programs_list

# url = "http://seafile.com/en/download/"
url = "https://thu10.alpha-sense.org/client/download"
logger = get_logger('seafile-updater')
PRODUCT_NAME = "Alphasense Sync"
COMPANY_NAME = "AlphaSense"
EXE_NAME = "seafile-applet.exe"
CCNET = "ccnet.exe"
DAEMON = "seaf-daemon.exe"
UPDATER_EXE_NAME = "updater.exe"
ASADMIN = 'asadmin'
USER_DENIED_ERROR_CODE = 1223


def getPath():
    sep = os.sep
    sfStart = "%s%s%sbin%s%s" % (sep, PRODUCT_NAME, sep, sep, EXE_NAME)
    getPath = os.environ["ProgramFiles"] + sfStart
    if not (os.path.exists(getPath)):
        getPath = os.environ["ProgramFiles(x86)"] + sfStart
        if not (os.path.exists(getPath)):
            raise Exception("Warning: Could not start alphasense sync")
    logger.info("Open ASFS from PATH %s" % getPath)
    return getPath


def getUpdaterExecutablePath():
    sep = os.sep
    sfStart = "%s%s%sbin%supdater%s%s" % (sep, PRODUCT_NAME, sep, sep, sep, UPDATER_EXE_NAME)
    getPath = os.environ["ProgramFiles"] + sfStart
    if not (os.path.exists(getPath)):
        getPath = os.environ["ProgramFiles(x86)"] + sfStart
        if not (os.path.exists(getPath)):
            raise Exception("Warning: Could not find updater in sync %s" % getPath)
    logger.info("Open updater from PATH %s" % getPath)
    return getPath


def dl(url):
    logger.info("Downloading from url %s" % (url))
    time_now = time.time()
    response = urllib2.urlopen(url)
    r = response.read()
    response.close()
    logger.info("Total time spent here %s minutes" % ((time.time() - time_now) / 60.0))
    return r


def list_executables(directory):
    return_list = []
    for name in os.listdir(directory):
        if name.lower().endswith('.exe'):
            return_list.append(name)
    return_list.sort()
    return return_list


def add_exe_to_startup_registry():
    try:
        cwd = os.getcwd()
        if 'updater' not in cwd:
            cwd = os.path.join(os.getcwd(), "bin", "updater")
        myExe_path = os.path.join(cwd, list_executables(cwd)[-1])
        logger.info("updater.exe full path while reg entry %s" % myExe_path)
    except:
        raise Exception('No executable found in this folder %s' % os.path.join(os.getcwd(), "bin", "updater"))
    logger.info("Updater Executable path %s" % myExe_path)
    try:
        from winreg_helper import AutoStartRegistry
        asr = AutoStartRegistry()
        if asr.exists("asfs_updater"):
            pass
        else:
            asr.add("asfs_updater", myExe_path)
    except WindowsError:
        logger.info("Windows error while reg entry")
    except Exception as e:
        logger.error("Error at winreg %s" % e.message)


def getVersion(addr):
    # https://thu10.alpha-sense.org/media/msi/alphasense-sync-en-1.0.5.msi
    last = addr.rfind('.')
    first = addr.rfind('-')+1
    return addr[first:last]


def getCurrentInstalledVersion():
    for p in get_installed_programs_list():
        if COMPANY_NAME in p.Vendor and p.Caption and COMPANY_NAME in p.Caption:
            return '{}'.format(p.Version)
    return None


def update(url, oldaddr):
    data = dl(url)
    regexp = "<a href=(.+).msi"
    regex = re.compile(regexp, re.MULTILINE)

    addr = regex.findall(data)[0] + ".msi"
    relay_version = getVersion(addr)
    current_version = getCurrentInstalledVersion()
    if addr == oldaddr or (current_version and relay_version <= current_version):  # Don't install same version again
        logger.info("No updates found. Skipping process")
        return addr
    f = tempfile.NamedTemporaryFile(suffix='.msi', delete=False)
    if shell.IsUserAnAdmin():
        logger.info("Now downloading the MSI")
        f.write(dl(addr))
        logger.info("MSI File Downloaded name=%s" % f.name)
    else:
        logger.info("No admin rights here , skip msi download")
    f.close()

    # Install the file
    # msiexec /passive /quiet /i alphasense-sync-en-1.0.5.msi /l*v %temp%\\msi.log /qn SILENTINSTALL=1
    cmd = "msiexec /passive /quiet /i %s %s /qn SILENTINSTALL=1" % (f.name, "/l*v %temp%\\msi.log")
    if shell.IsUserAnAdmin():
        logger.info("adding executable to startup registry")
        add_exe_to_startup_registry()
        logger.info("Trying to install the product as admin")
        try:
            last_updated, interval_in_hours, _ = loadConfig()
            logger.info("last_updated [%s] interval [%s]" % (last_updated, interval_in_hours))
            saveConfig(interval_in_hours, installed_addr)
        except Exception, e:
            logger.error("exception has occured while save config")
        installer_status_code = Popen(cmd, shell=True).wait()
    else:
        params = ' '.join([ASADMIN])
        import win32con
        showCmd = win32con.SW_SHOWNORMAL
        logger.info("No admin rights here , trying to elevate for admin rights")
        hinstance = None
        try:
            hinstance = shell.ShellExecuteEx(nShow=showCmd,
                                             lpVerb='runas',
                                             lpFile=sys.executable,
                                             lpParameters=params)
        except pywintypes.error, e:
            if USER_DENIED_ERROR_CODE == e.winerror:
                logger.info("No admin rights given here , will trying to elevate After %d hours" % INTERVAL_IN_HRS)
                raise Exception("No admin rights given here")
        except Exception as e:
            logger.info("No admin rights given here , will trying to elevate After %d hours" % INTERVAL_IN_HRS)
            raise Exception("No admin rights given here")
        code = -1
        # hinstance = shell.ShellExecuteEx(lpVerb='runas', lpFile=sys.executable, lpParameters=params)
        if not hinstance:
            logger.info("No drivers found to install as admin")
            pass
        else:
            code = hinstance['hInstApp']
            logger.info("Self elevating return code %d" % code)
        if code > 32:
            logger.info("admin rights provided, now running separate updater.exe")
            logger.info("Exiting updater.exe now")
            installer_status_code = 112
            os.system("exit")
            sys.exit(0)
        else:
            logger.info("No admin rights given here , will trying to elevate After %d hours" % INTERVAL_IN_HRS)
            installer_status_code = 111
    logger.info("cmd [%s]" % cmd)

    if installer_status_code != 0:
        raise Exception("Installing went wrong error code = [%d]" % installer_status_code)

    logger.info("Return error code is 0 ,True=[%d]" % (installer_status_code == 0))
    logger.info("Address is [%s]" % addr)

    return addr


def process_exists(name):
    import psutil
    for proc in psutil.process_iter():
        try:
            if proc.name() == name:
                return True
        except psutil.AccessDenied:
            print "Permission error or access denied on process"
            return False
    return False


def check_if_product_not_running():
    PRODUCT_PROCESSES = [EXE_NAME]
    try:
        for each_process in PRODUCT_PROCESSES:
            if process_exists(each_process):
                return True
    except Exception as e:
        logger.error("Error %s %s" % e, e.message)
        return False
    return False


if __name__ == "__main__":
    logger.info("#####################################################")
    logger.info("###########             START             ###########")
    logger.info("#####################################################")

    if not "Windows" in platform.platform():
        raise Exception("The autoupdate only works on windows")

    # product_running = check_if_product_not_running()
    if shell.IsUserAnAdmin():
        logger.info("At Updater startup with admin rights")
    else:
        logger.info("At Updater startup without admin rights")

    last_updated, interval_in_hours, installed_addr = loadConfig()

    if not confExisted:  # Updated directly first time
        logger.info("No config file found setting interval_in_hours to %d" % interval_in_hours)
        interval_in_hours = 0

    while True:
        passedtime = timeSinceLastCheck(last_updated) if last_updated else 0
        logger.info("last update checked before %s minutes" % (passedtime / 60))
        logger.info("Now Sleeping for %s minutes" % (max(interval_in_hours * 3600 - passedtime, 0) / 60))
        time.sleep(max(interval_in_hours * 3600 - passedtime, 0))
        try:
            installed_addr = update(url, installed_addr)
            logger.info("Installed Directory %s" % (installed_addr))
        except Exception, e:
            logger.error("Error %s" % e.message)
            print e

        try:
            last_updated, interval_in_hours, _ = loadConfig()
            logger.info("last_updated [%s] interval [%s]" % (last_updated, interval_in_hours))
            saveConfig(interval_in_hours, installed_addr)
            last_updated, interval_in_hours, _ = loadConfig()
        except Exception, e:
            logger.error("exception has occured while save config")
