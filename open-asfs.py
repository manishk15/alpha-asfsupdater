# -*- coding: utf-8 -*-

"""
    Copyright
    -----------------------------------------------------------------------------
    Copyright (C) Talentica Software , Inc - All Rights Reserved Unauthorized 
    copying of this file, via any medium is strictly prohibited
    -----------------------------------------------------------------------------

    FileName :      open-asfs.py
    Created Date :  26-12-2016
    Created by :    MrityunjayK mkumar@alpha-sense.com
"""

"""
    Shared Variables
"""
from os import sep, environ
from os.path import exists
from subprocess import Popen
"""
    Local Functions
"""

"""
    Global Functions
"""

"""
    Class Definitions
"""
PRODUCT_NAME = "Alphasense Sync"
EXE_NAME = "seafile-applet.exe"
"""
This function only opens the file sync applet in non-admin mode
"""

def getPath():
    sfStart = "%s%s%sbin%s%s" % (sep, PRODUCT_NAME, sep, sep, EXE_NAME)
    getPath = environ["ProgramFiles"] + sfStart
    if not (exists(getPath)):
        getPath = environ["ProgramFiles(x86)"] + sfStart
        if not (exists(getPath)):
            raise Exception("Warning: Could not start alphasense sync")
    return getPath

if __name__ == "__main__":
    from platform import platform
    if not "Windows" in platform():
        raise Exception("The autoupdate only works on windows")
    try:
        cmd = "\"%s\"" % getPath()
        Popen(cmd)
    except Exception as e:
        print e,e.message