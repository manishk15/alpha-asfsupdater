## Alphasense File Sync Updater

[Alphasense](https://alpha-sense.com) File Sync Updater.

## BUILD ##

### Prerequisites ###
- Windows system (7 or later)
- [python](https://www.python.org/ftp/python/2.7.12/python-2.7.12.msi)

```sh
    python -m pip install pypiwin32
    python -m pip install future --upgrade
```

### Making the build ###

Please run the build.bat


Repo Owner : @mrityunjaykumar 